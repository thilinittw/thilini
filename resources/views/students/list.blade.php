@extends('layouts.app')

@section('content')
<link href="{{URL('css/prettyPhoto.css')}}" rel="stylesheet">
<script src="{{URL('js/jquery.prettyPhoto.js')}}"></script>
<div class="panel panel-default">
    <div class="col-sm-12">
        <h4 id="overview" class="page-header"><a href="#"><strong> {{  strtoupper('Students')}}</strong></a></h4>
    </div>
    
    <div class="panel-body">
        <br/>
        <br/>
        @if(count($students)>0)
        <table class="table  table-bordered students_table">
            <thead>
                <tr>
                    <th >Name</th>
                    <th >Address</th>
                    <th >Grade</th>
                    <th >Classe</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($students as $student)
                <tr>
                    <td>{{$student->student_name}}</td>
                    <td >{{$student->student_address}}</td>
                    <td >{{$student->grade}}</td>
                    <td >{{$student->classe_name}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        No records found
        
        @endif
        
    </div>
</div>
<script>
$(document).ready(function(){
 $('.students_table').DataTable();
});
</script>
@endsection
