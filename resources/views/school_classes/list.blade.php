@extends('layouts.app')

@section('content')
<link href="{{URL('css/prettyPhoto.css')}}" rel="stylesheet">
<script src="{{URL('js/jquery.prettyPhoto.js')}}"></script>
<div class="panel panel-default">
    <div class="col-sm-12">
        <h4 id="overview" class="page-header"><a href="#"><strong> {{  strtoupper('classes')}}</strong></a></h4>
    </div>
    
    <div class="panel-body">
        <div class="row">
            <a href="{{URL('classes/add')}}" class="btn btn-primary pull-right">Add new Class</a>
        </div>
        
        @if(count($classes)>0)

        <table class="table  table-bordered classes_table">
            <thead>
                <tr>
                    <th >Classe Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($classes as $classe)
                <tr>
                    <td >{{$classe->classe_name}}</td>
                    <td>
                        <a href="{{URL('students/add/'.$classe->classe_id)}}"> Add students</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        No records found
        
        @endif
        
    </div>
</div>
<script>
$(document).ready(function(){
    $('.students_table').DataTable();
});

</script>    
@endsection
