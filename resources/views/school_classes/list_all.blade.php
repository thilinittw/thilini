@extends('layouts.app')

@section('content')
<link href="{{URL('css/prettyPhoto.css')}}" rel="stylesheet">
<script src="{{URL('js/jquery.prettyPhoto.js')}}"></script>
<div class="panel panel-default">
    <div class="col-sm-12">
        <h4 id="overview" class="page-header"><a href="#"><strong> {{  strtoupper('hierarchical view')}}</strong></a></h4>
    </div>
    <br/>
        <br/>
    <div class="panel-body">
        <br/>
        <br/>
        
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            @if(count($details)>0)
            @foreach($details as $detail)
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading{{$detail['gradeId']}}">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$detail['gradeId']}}" aria-expanded="true" aria-controls="collapseOne">
                          {{$detail['grade']}}
                        </a>
                    </h4>
                </div>
                <div id="collapse{{$detail['gradeId']}}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading{{$detail['gradeId']}}">
                    <div class="panel-body">
                            <ul>
                                @if(count($detail['classes'])>0)
                                @foreach($detail['classes'] as $classe)
                                    <li>{{$classe->classe_name}}</li>
                                @endforeach  
                                @endif 
                            </ul>
                    </div>
                </div>
            </div>
            @endforeach
            @else
            No records found
            @endif
        </div>
        
    </div>
</div>
<script>
$(document).ready(function(){
 $("a[rel^='prettyPhoto']").prettyPhoto({
     social_tools: false,
     allow_expand: false,
     
 });
});

//block/unblock confirmation
function delete_confirmation()
{
    var result = confirm("Do you want to delete this reward?");
    if(result)
    {
        return true;
    }
    else
    {
        return false;
    }
}
</script>    
@endsection
