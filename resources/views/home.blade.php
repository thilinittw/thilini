@extends('layouts.app')

@section('content')
<style>
    .dimension_table th{
        text-align: center;
    }
</style>
<div class="panel panel-default">
    <div class="col-sm-12">
        <h4 id="overview" class="page-header"><a href="#"><strong>8 {{  strtoupper('Dimensions')}}</strong></a></h4>
    </div>
    
    <div class="panel-body">
        <table class="table table-bordered dimension_table">
            <thead>
                <tr>
                    <th >Dimension</th>
                    <th >Description</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div>
    
</div>

@endsection
