<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Grade;
use App\Models\SchoolClass;
use App\Models\Student;


class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         $this->middleware('auth');
    }
    
    /**
     * Show add grades page
     *
     * @return \Illuminate\Http\Response
     */
    public function add($id,Request $request)
    {
        //take Grades
        $data['classe'] = SchoolClass::find($id);
        return view('students.add',$data);
    }

    /**
     * list class students 
     *
     * @return \Illuminate\Http\Response
     */
    public function listStudents($id,Request $request)
    {
        $data['students'] = Student::join('school_classes','school_classes.classe_id','=','students.classe_id')
                                    ->join('grades','grades.grade_id','=','school_classes.grade_id')
                                    ->where('students.classe_id',$id)
                                    ->select('students.*','grade','classe_name')
                                    ->get();
        return view('students.list',$data);
    }

    /**
     * list all students 
     *
     * @return \Illuminate\Http\Response
     */
    public function listAllStudents(Request $request)
    {
        $data['students'] = Student::join('school_classes','school_classes.classe_id','=','students.classe_id')
                                    ->join('grades','grades.grade_id','=','school_classes.grade_id')
                                    ->select('students.*','grade','classe_name')
                                    ->get();
        return view('students.list',$data);
    }

    /**
     * save grades details
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $v = $this->validate($request, [
            'studentName'    => 'required',
            'studentAddress' => 'required',
            'classId'        => 'required',
        ]);
        $classe = $request->input('classId');
        
        $student                  = new Student;
        $student->classe_id       = $classe;
        $student->student_name    = $request->input('studentName');
        $student->student_address = $request->input('studentAddress');
        $student->save();
        return redirect('students/all/list');
        
    }
    
    /**
     * delete classes
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteClasses($id,Request $request)
    {
        ScoolClass::find($id)->delete();
        return redirect('classes/list');
    }
    
    
}

