<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Reward;


class RewardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show add rewards page
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('rewards.add_rewards');
    }
    /**
     * block/unblock user
     *
     * @return \Illuminate\Http\Response
     */
    public function listRewards(Request $request)
    {
        $data['rewards'] = Reward::all();
        return view('rewards.rewards_list',$data);
    }
    
    /**
     * save new reward details
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        
        $v = $this->validate($request, [
            'reward_name' => 'required',
            'banner_img' => 'required',
            'website' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
        ]);
        
        $reward            = new Reward;
        $reward->name      = $request->input('reward_name');
        $reward->website   = $request->input('website');
        $banner_img        = $request->file('banner_img');
        $file              = md5(time()) . '.png';
        $banner_img->move(public_path('images/banners'),$file);
        $reward->image_url = url('images/banners') . '/' . $file;
        $reward->save();
        return redirect('rewards/list');
        
    }
    
    /**
     * delete reward details
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteRewards($id,Request $request)
    {
        Reward::find($id)->delete();
        return redirect('rewards/list');
    }
    
    
}

