<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\Grade;
use App\Models\SchoolClass;
use App\Models\Student;
use App\Models\User;
use Response;
use Validator;

class SchoolClasseController extends Controller
{


    /**
     * list added grades 
     *
     * @return \Illuminate\Http\Response
     */
    public function listAll(Request $request)
    {

        if(!$this->checkUserLogin($request))//check user login
        {
            return Response::json([
                'result'  => [],
                'error' => 'InvalidTokent',
                'status'   => 'ERROR'
            ],401);
        }
        $result = [];
        //take Grades
        $grades = Grade::get();
        $i=0;
        //Grades exist
        if(count($grades)>0)
        {
            foreach ($grades as $grade) 
            {
                $gradeId = $grade->grade_id;
                $result[$i]['grade'] = $grade;
                //take grade realted classes
                $classes = SchoolClass::where('grade_id',$gradeId)->get();
                $result[$i]['classes'] = $classes;
                $i++;
            }
        }
        return Response::json([
            'result'  => $result,
            'error' => '',
            'status'   => 'SUCCESS'
        ],200);
    }

    
    /**
     * delete classes
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteClasses(Request $request)
    {
        if(!$this->checkUserLogin($request))//check user login
        {
            return Response::json([
                'result'  => [],
                'error' => 'InvalidTokent',
                'status'   => 'ERROR'
            ],401);
        }

        //validate classe id
        $validator = Validator::make($request->all(), [
            'classId'        => 'required',
        ]);
        if ($validator->fails()) {
            return Response::json([
                'result'  => [],
                'error'   => 'RequiredFieldsNull',
                'status'  => 'ERROR'
            ],422);
        }

        //remove all students
        Student::where('classe_id',$request->classeId)->delete();
        SchoolClass::find($request->classId)->delete();
        return Response::json([
            'result'  => '',
            'error' => '',
            'status'   => 'SUCCESS'
        ],200);
    }

    /*
    *check user login
    */
    public function checkUserLogin(Request $request)
    {
        if(isset($request->token)&& ($request->token!=''))
        {
            $user = User::where('token',$request->token)->first();
            if(count($user)>0)
            {
                return true;
            }
            else 
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    
    
}

