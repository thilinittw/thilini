<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $table       = 'grades';
    protected $primaryKey  = 'grade_id';
    protected $fillable    = ['grade'];
    protected $hidden      = ['created_at','updated_at'];
    
}
