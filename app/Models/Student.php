<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table       = 'students';
    protected $primaryKey  = 'student_id';
    protected $fillable    = ['student_name','dob','address','class_id'];
    protected $hidden      = ['created_at','updated_at'];
    
}
