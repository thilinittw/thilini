<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolClass extends Model
{
    protected $table       = 'school_classes';
    protected $primaryKey  = 'class_id';
    protected $fillable    = ['class'];
    protected $hidden      = ['created_at','updated_at'];
    
}
